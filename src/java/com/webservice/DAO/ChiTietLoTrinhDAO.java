/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author TIEN
 */
public class ChiTietLoTrinhDAO {
    public static ResultSet getChiTietLoTrinh(String MaNhanVien, String NgayPhanCa) {
        System.out.println("-------------Chi tiết lộ trình làm việc của :\nMa nhân viên: " + MaNhanVien + "\nNgay phan ca:" + NgayPhanCa);
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callpro = con.getConnect().prepareCall("{call sms_chitiet_lotrinh_final(?, ?)}");
            callpro.setString(1, MaNhanVien);
            callpro.setString(2, NgayPhanCa);
            ResultSet result = callpro.executeQuery();
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("-------lỗi get chi tiết lộ trình call procedure-----------");
        }
        System.out.println("//////Không có lộ trình làm việc//////");
        return null;
    }
     
}
