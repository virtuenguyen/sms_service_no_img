/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 * @author TIEN
 */
public class CongViecTaiMotDiemDAO {

    public static ResultSet getCongViecTaiMotDiem(String MaNhanVien, int MaDiaDiem, int MaLoTrinh) {
        System.out.println("-------------Kiểm tra lịch làm việc của :\nMa nhân viên: " + MaNhanVien);
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callpro = con.getConnect().prepareCall("{call sp_CongViecTaiMotDiem_Services(?,?,?)}");
            callpro.setString(1, MaNhanVien);
            callpro.setInt(2, MaDiaDiem);
            callpro.setInt(3, MaLoTrinh);
            ResultSet result = callpro.executeQuery();
            return result;
        } catch (Exception ex) {
            System.out.println("-------loi check lich lam viec call procedure-----------");
        }
        System.out.println("//////Nhân viên này không co lich lam viec//////");
        return null;
    }
}
