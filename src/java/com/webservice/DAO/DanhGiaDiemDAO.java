/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import com.webservice.objects.DTO_AN_DanhGia;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author TIEN
 */
public class DanhGiaDiemDAO {

    public static boolean insertDanhGiaDiaDiem(ArrayList<DTO_AN_DanhGia> list) {
        System.out.println("--------------- bắt đầu insert đánh giá -----------------");
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call an_DanhGia_insert(?,?,?,?,?)}");
            for (DTO_AN_DanhGia danhGia : list) {
                System.out.println("---------ma_kq  id diem: " + danhGia.getId_diem());
                callproc.setInt(1, danhGia.getId_diem());               
                System.out.println("---------Nội dung đánh giá: " + danhGia.getNoi_dung_danh_gia());
                callproc.setString(2, danhGia.getNoi_dung_danh_gia());
                System.out.println("---------hình ảnh: " + danhGia.getAnh_chup());
                callproc.setBytes(3, danhGia.getAnh_chup());
                System.out.println("---------kết quả đánh giá: " + danhGia.getKet_qua_danh_gia());
                callproc.setString(4, danhGia.getKet_qua_danh_gia());
                System.out.println("---------Chú thích: " + danhGia.getChu_thich());
                callproc.setString(5, danhGia.getChu_thich());
                callproc.addBatch();
                System.out.println("---------------du lieu insert -----------------");
            }
            callproc.executeBatch();
            System.out.println("---------------insert du lieu bang ket qua thanh cong------------");
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---------------co loi trong qua trinh insert ket qua------------");
        }
        return false;
    }

}
