/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import com.webservice.objects.DTO_AN_ChupAnh;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author TIEN
 */
public class KetQuaChupHinhDAO {

    public static boolean insertKetQuaChupHinh(ArrayList<DTO_AN_ChupAnh> list) {
        System.out.println("--------------- bắt đầu insert -----------------");
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call an_ChupAnh_insert(?,?,?)}");
            for (DTO_AN_ChupAnh ketqua : list) {
                System.out.println("---------ma_đia điểm: " + ketqua.getId_Diem());
                callproc.setInt(1, ketqua.getId_Diem());
                System.out.println("---------Camera trước: " + ketqua.getCamera_truoc());
                callproc.setBytes(2, ketqua.getCamera_truoc());
                System.out.println("---------Camera sau: " + ketqua.getCamera_sau());
                callproc.setBytes(3, ketqua.getCamera_sau());
                callproc.addBatch();
                System.out.println("---------------dữ liệu insert -----------------");
            }
            callproc.executeBatch();
            System.out.println("---------------insert du lieu bang ket qua thanh cong------------");
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---------------co loi trong qua trinh insert ket qua o procedure------------");
        }
        return false;
    }
}
