/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import com.webservice.objects.DTO_AN_Diem;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author TIEN
 */
public class KetQuaDiemDAO {

    public ResultSet insert_DAO_AN_Diem(DTO_AN_Diem dto) {
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement call = con.getConnect().prepareCall("call an_Diem_insert(?,?,?,?,?,?,?,?,?)");
            call.setString(1, dto.getKiem_soat_thoi_gian());
            call.setString(2, dto.getTen_diem());
            call.setString(3, dto.getThoi_gian_yeu_cau_quyet_the());
            call.setString(4, dto.getThoi_gian_quyet_thuc_te());
            call.setInt(5, dto.getThu_tu());
            call.setInt(6, dto.getThoi_gian_thuc_hien());
            call.setInt(7, dto.getTrang_thai_nhan_vien());
            call.setInt(8, dto.getMa_lo_trinh());
            call.setInt(9, dto.getTrang_thai_danh_gia());
//            call.setInt(12, dto.getMa_diem1());
//            call.setInt(13, dto.getMa_diem2());
            return call.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
