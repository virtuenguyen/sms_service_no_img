/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import com.webservice.objects.DTO_AN_LoTrinh;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author TIEN
 */
public class KetQuaLoTrinhDAO {

    public ResultSet insert_DAO_AN_LoTrinh(DTO_AN_LoTrinh ketQuaAN_LoTrinh) {
        System.out.println("--------------- bắt đầu insert -----------------");
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call an_LoTrinh_insert(?,?,?,?,?,?,?,?,?,?,?,?)}");
            System.out.println("---------mã lộ trinh: " + ketQuaAN_LoTrinh.getMa_lo_trinh());
            callproc.setInt(1, ketQuaAN_LoTrinh.getMa_lo_trinh());
            System.out.println("---------tên lộ trình: " + ketQuaAN_LoTrinh.getTen_lo_trinh());
            callproc.setString(2, ketQuaAN_LoTrinh.getTen_lo_trinh());
            System.out.println("---------ma nhân viên: " + ketQuaAN_LoTrinh.getMa_nhan_vien());
            callproc.setInt(3, ketQuaAN_LoTrinh.getMa_nhan_vien());
            System.out.println("---------thời gian bắt đầu: " + ketQuaAN_LoTrinh.getThoi_gian_bat_dau());
            callproc.setString(4, ketQuaAN_LoTrinh.getThoi_gian_bat_dau());
            System.out.println("---------ngày phân ca: " + ketQuaAN_LoTrinh.getNgay_phan_ca());
            callproc.setString(5, ketQuaAN_LoTrinh.getNgay_phan_ca());
            System.out.println("---------số điẻm: " + ketQuaAN_LoTrinh.getSo_diem());
            callproc.setInt(6, ketQuaAN_LoTrinh.getSo_diem());
            System.out.println("---------trạng thái đánh giá lộ trình: " + ketQuaAN_LoTrinh.getTrang_thai_danh_gia_lo_trinh());
            callproc.setInt(7, ketQuaAN_LoTrinh.getTrang_thai_danh_gia_lo_trinh());
            System.out.println("---------thời gian lộ trình: " + ketQuaAN_LoTrinh.getThoi_gian_lo_trinh());
            callproc.setInt(8, ketQuaAN_LoTrinh.getThoi_gian_lo_trinh());
            System.out.println("---------thời gian nghỉ: " + ketQuaAN_LoTrinh.getThoi_gian_nghi());
            callproc.setInt(9, ketQuaAN_LoTrinh.getThoi_gian_nghi());
            System.out.println("---------Mã phân công: " + ketQuaAN_LoTrinh.getMa_phan_cong());
            callproc.setInt(10, ketQuaAN_LoTrinh.getMa_phan_cong());
            System.out.println("---------Vòng: " + ketQuaAN_LoTrinh.getVong());
            callproc.setInt(11, ketQuaAN_LoTrinh.getVong());
            System.out.println("--------Thứ tự: " + ketQuaAN_LoTrinh.getThu_tu_lo_trinh());
            callproc.setInt(12, ketQuaAN_LoTrinh.getThu_tu_lo_trinh());

            System.out.println("/////Insert thanh cong/////");
           return callproc.executeQuery();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("---------------có lỗi trong quá trình insert ------------");
        }
//        return false;
        return null;
    }

    public  ResultSet searchMaPhanCong(int maNhanVien, String ngayPC) {
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callpro = con.getConnect().prepareCall("{call sms_Sevice_searchMaPC(?,?)}");
            callpro.setInt(1, maNhanVien);
            callpro.setString(2, ngayPC);
            return callpro.executeQuery();
        } catch (Exception ex) {
            System.out.println("-------Lỗi call procedure-----------");
        }
        System.out.println("//////Nhân viên này không được phân công//////");
        return null;
    }

}
