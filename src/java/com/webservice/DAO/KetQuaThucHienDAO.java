/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import com.webservice.objects.KetQuaChupHinhObj;
import com.webservice.objects.KetQuaThucHienObj;
import java.sql.CallableStatement;

/**
 *
 * @author TIEN
 */
public class KetQuaThucHienDAO {

    public static boolean insertKetQuanThucHien(KetQuaThucHienObj ketQuaThucHien) {
        System.out.println("--------------- bắt đầu insert -----------------");
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call sms_KetQuanThucHien_Insert(?,?,?,?,?)}");
            System.out.println("---------mã phân công: " + ketQuaThucHien.getMa_phan_cong());
            callproc.setInt(1, ketQuaThucHien.getMa_phan_cong());
            System.out.println("---------mã điểm: " + ketQuaThucHien.getMa_diem());
            callproc.setInt(2, ketQuaThucHien.getMa_diem());
            System.out.println("---------thời gian yêu cầu quẹt thẻ: " + ketQuaThucHien.getTg_yeu_cau_quet_the());
            callproc.setString(3, ketQuaThucHien.getTg_yeu_cau_quet_the());
            System.out.println("---------trạng thái quẹt thẻ: " + ketQuaThucHien.getTrang_thai_quet_the());
            callproc.setString(4, ketQuaThucHien.getTrang_thai_quet_the());
            System.out.println("---------thời gian quẹt thẻ thực tế: " + ketQuaThucHien.getTg_quet_the_thuc_te());
            callproc.setString(5, ketQuaThucHien.getTg_quet_the_thuc_te());
            callproc.executeUpdate();
            System.out.println("/////Insert thanh cong/////");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("---------------có lỗi trong quá trình insert ------------");
        }
        return false;
    }
}
