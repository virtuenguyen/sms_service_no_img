/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import java.sql.CallableStatement;
import java.sql.ResultSet;

/**
 *
 * @author TIEN
 */
public class LichLamViecDAO {
    public static ResultSet checkLichLamViec(String ma_nhan_vien, String thang_pc) {
        System.out.println("-------------Kiểm tra lịch làm việc của :\nMa nhân viên: " + ma_nhan_vien + "\nTháng phân ca:" + thang_pc  );
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callpro = con.getConnect().prepareCall("{call sms_NhanVien_LichLamViec_Service(?, ?)}");
            callpro.setString(1, ma_nhan_vien);
            callpro.setString(2, thang_pc);
            ResultSet result = callpro.executeQuery();
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("-------lỗi check lịch làm việc ở call procedure-----------");
        }
        System.out.println("//////Nhân viên này không có lịch làm việc//////");
        return null;

    }
}