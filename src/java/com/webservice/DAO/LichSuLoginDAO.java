/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import com.webservice.utilities.FormatTime;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 *
 * @author TIEN
 */
public class LichSuLoginDAO {
    public static void insert(int MaNhanVien, Timestamp tgserver, String tg, String ha) {
        Timestamp tgandroid = FormatTime.convert(tg);
        byte[] hinhanh = null;
        if (!ha.equalsIgnoreCase("NO")) {
//            hinhanh =org.apache.commons.codec.binary.Base64.decodeBase64(ha);
        }
        System.out.println("--------------- bat dau insert lich su dang nhap-----------------");
        try {
            ConnectDB con=new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call spLichSu_Insert(?,?,?,?)}");
            System.out.println("---------ma nhan vien: " + MaNhanVien);
            callproc.setInt(1, MaNhanVien);
            System.out.println("---------thoi gian server: " + tgserver);
            callproc.setTimestamp(2, tgserver);
            System.out.println("---------thoi gian dang nhap: " + tgandroid);
            callproc.setTimestamp(3, tgandroid);
            callproc.setBytes(4, hinhanh);
            callproc.executeUpdate();
            System.out.println("/////Insert lich su thanh cong/////");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("/////Co loi trong qua trinh insert lich su/////");
        }
    }
    
}
