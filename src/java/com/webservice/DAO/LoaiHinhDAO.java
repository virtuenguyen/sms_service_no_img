/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import java.sql.CallableStatement;

/**
 *
 * @author TIEN
 */
public class LoaiHinhDAO {
     public static boolean insertLoaiHinh(String ten_loai_hinh) {
        System.out.println("--------------- bắt đầu insert -----------------");
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call sms_LoaiHinh_Insert(?)}");
            System.out.println("---------ten_loai_hinh: " + ten_loai_hinh);
            callproc.setString(1, ten_loai_hinh);
            callproc.executeUpdate();
            System.out.println("/////insert thành công/////");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("---------------có lỗi trong quá trình insert ------------");
        }
        return false;
    }
}
