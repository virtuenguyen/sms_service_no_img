/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.DAO;

import com.webservice.connectdb.ConnectDB;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author TIEN
 */
public class NhanVienDAO {
    public static ResultSet checkLoginNV(String ten_dang_nhap, String mat_khau) {
        System.out.println("-------------kiem tra dang nhap voi user: " + ten_dang_nhap + "-------mat khau: " + mat_khau);
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callpro = con.getConnect().prepareCall("{call sms_NhanVien_Login_Services(?,?)}");
            callpro.setString(1, ten_dang_nhap);
            callpro.setString(2, mat_khau);
           return callpro.executeQuery();
        } catch (Exception ex) {
            System.out.println("-------loi check login phan call procedure-----------");
        }
        return null;
    }
//    public static ResultSet checkLogin(String TenDangNhap, String MatKhau){
//        System.out.println("-------------kiem tra dang nhap voi user: " + TenDangNhap + "-------mat khau: " + MatKhau);
//    }

    public static boolean changePass(int ma_nhan_vien, String mat_khau) {
        System.out.println("-------------Change pass:\nMa nhan vien: " + ma_nhan_vien + "-------mat khau: " + mat_khau);
        try {
            ConnectDB con = new ConnectDB();
            CallableStatement callproc = con.getConnect().prepareCall("{call spNhanVien_ChangePass(?,?)}");
            callproc.setInt(1, ma_nhan_vien);
            callproc.setString(2, mat_khau);
            callproc.executeUpdate();
            callproc.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("-------loi call procedure change pass-----------");
        }
        return false;
    }
    
}
