/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.LichCongViecConvert;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.webservice.objects.LichCongViecObj;

/**
 *
 * @author TIEN
 */

@XmlRootElement(name = "lichlamviec")
public class LichCongViecConvert {

    private LichCongViecObj entity = null;

    public LichCongViecConvert() {
        entity = new LichCongViecObj();
    }

    public LichCongViecConvert(LichCongViecObj entity) {
        this.entity = entity;
    }

    @XmlElement
    public String getMaNhanVien() {
        return entity.getMaNhanVien();
    }

    @XmlElement
    public String getNgayPhanCa() {
        return entity.getNgayPhanCa();
    }

    @XmlElement
    public int getMaLoTrinh() {
        return entity.getMaLoTrinh();
    }

    public LichCongViecObj getLichCongViecObj() {
        return entity;
    }

    public void setMaNhanVien(String MaNhanVien) {
        entity.setMaNhanVien(MaNhanVien);
    }

    public void setNgayPhanCa(String NgayPhanCa) {
        entity.setNgayPhanCa(NgayPhanCa);
    }

    public void setMaLoTrinh(int MaLoTrinh) {
        entity.setMaLoTrinh(MaLoTrinh);
    }

}
