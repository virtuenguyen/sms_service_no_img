/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.connectdb;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author TIEN
 */
public class ConnectDB {

    private Connection conn = null;
    private String db_user = "root";
//    private String db_password = "root";
//    private String db_url = "jdbc:mysql://localhost:3306/db_sms_demo_no_image?characterEncoding=UTF-8";
    
    private String db_password = "r123";
    private String db_url =  "jdbc:mysql://127.0.0.1:3306/db_sms_demo_no_image?characterEncoding=UTF-8";
    private String db_driver = "com.mysql.jdbc.Driver";
    public ConnectDB() {
        openConnection();
    }

    private void openConnection() {
        try {
            Class.forName(db_driver);
            conn = DriverManager.getConnection(db_url, db_user, db_password);
            System.out.println("-------------------------connected databse----------------------");
        } catch (Exception e) {
            System.out.println("-------------------------can not connect databse----------------------");
            System.out.println(e);
            e.printStackTrace();
        }
    }

    public Connection getConnect() {
        return conn;
    }
}
