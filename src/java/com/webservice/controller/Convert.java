/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.controller;

import com.webservice.LichCongViecConvert.LichCongViecConvert;

import javax.ws.rs.ext.ContextResolver;

import javax.ws.rs.ext.Provider;

import javax.xml.bind.JAXBContext;

import com.sun.jersey.api.json.JSONConfiguration;

import com.sun.jersey.api.json.JSONJAXBContext;

@Provider

public class Convert implements ContextResolver< JAXBContext> {

    private JAXBContext context;

    private Class[] types = {LichCongViecConvert.class};
    
    
    public Convert() throws Exception {
        this.context = new JSONJAXBContext(JSONConfiguration.mapped().arrays("lichlamviec").build(),
                types);


    }
    public JAXBContext getContext(Class<?> objectType) {
        System.out.println("callllllllCALL");
        for (Class type : types) {
            if (type == objectType) {
                return context;

            }

        }

        return null;

    }

}
