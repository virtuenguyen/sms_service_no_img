/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.controller;

import com.webservice.DAO.DanhGiaDiemDAO;
import com.webservice.DAO.KetQuaChupHinhDAO;
import com.webservice.LichCongViecConvert.LichCongViecConvert;
import com.webservice.db.ChiTietLoTrinh;
import com.webservice.db.CongViecTaiMotDiem;
import com.webservice.db.KetQua;
import com.webservice.db.LichCongViec;
import com.webservice.db.NhanVien;
import com.webservice.objects.ChiTietLoTrinhObj;
import com.webservice.objects.CongViecTaiMotDiemObj;
import com.webservice.objects.DanhGiaDiemObj;
import com.webservice.objects.KetQuaChupHinhObj;
import com.webservice.objects.KetQuaThucHienObj;
import com.webservice.objects.LichCongViecObj;
import com.webservice.objects.LoaiHinhObj;
import com.webservice.objects.NhanVienObj;
import com.webservice.utilities.ClassConvertData;
import com.webservice.utilities.Class_EncryptedDecrypted;
import com.webservice.utilities.FormatTime;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author TIEN
 */
@Path("/nhanvien")
public class NhanVienCtrl {

//    @Context
//    UriInfo uriInfo;
//    @Context
//    Request request;
    @POST
    @Path("/dangnhap")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

    public NhanVienObj checkLogin(MultivaluedMap<String, String> param) {
        String id = param.getFirst("username");
        String pass = param.getFirst("pass");
//        String tg = param.getFirst("thoigian");
//        String ha = param.getFirst("hinhanh");
        NhanVienObj obj = NhanVien.checkLoginV2(id, new Class_EncryptedDecrypted().Encrypted(pass));
        return obj;
    }

    @POST
    @Path("/lichlamviec")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ArrayList<LichCongViecConvert> chekLichLamViec(MultivaluedMap<String, String> param) {
        String id = param.getFirst("id");
        String thang = param.getFirst("thang");
        ArrayList<LichCongViecConvert> arr = LichCongViec.CheckLichLamViec(id, thang);
        return arr;

    }
    
    @POST
    @Path("/chitietlotrinh")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ArrayList<ChiTietLoTrinhObj> getListChiTietLoTrinh(MultivaluedMap<String, String> param) {
        String id = param.getFirst("id");
        String ngay = param.getFirst("ngay");
        ArrayList<ChiTietLoTrinhObj> arr = ChiTietLoTrinh.getListChiTietLoTrinh(id, ngay);
        return arr;
    }

    @POST
    @Path("/danhsachlotrinh")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ArrayList<ChiTietLoTrinhObj> getListLoTrinh(MultivaluedMap<String, String> param) {
        String id = param.getFirst("id");
        String ngay = param.getFirst("ngay");
        ArrayList<ChiTietLoTrinhObj> arr = ChiTietLoTrinh.getListChiTietLoTrinh(id, ngay);
        return arr;
    }

    @GET
    @Path("/chitietlotrinh1/{id}/{ngay}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<ChiTietLoTrinhObj> getMessagePathParam(@PathParam("id") String id,
        @PathParam("ngay") String ngay) {
        ArrayList<ChiTietLoTrinhObj> arr = ChiTietLoTrinh.getListChiTietLoTrinh(id, ngay);
        return arr;
    }

    @POST
    @Path("/upload_diem")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String uploadlotrinh(MultivaluedMap<String, String> param) {
        KetQua kq = new KetQua();
        String id = param.getFirst("ketqua");
        String ketqua = kq.insert(id);
        System.out.println("ok");
        return ketqua;
    }

}
//    @POST
//    @Path("/congviectaimotdiem")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
//    public CongViecTaiMotDiemObj getCongViecTaiMotDiem(MultivaluedMap<String, String> param) {
//        String idNV = param.getFirst("id");
//        String idLT = param.getFirst("idLT");
//        String idDD = param.getFirst("idDD");
//
//        CongViecTaiMotDiemObj obj = CongViecTaiMotDiem.getCongViecTaiMotDiem(idNV, new ClassConvertData().ConvertStringToInt(idLT), new ClassConvertData().ConvertStringToInt(idDD));
//        return obj;
//    }
