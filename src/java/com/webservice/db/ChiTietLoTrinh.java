/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.db;

import com.webservice.DAO.ChiTietLoTrinhDAO;
import com.webservice.objects.ChiTietLoTrinhObj;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author TIEN
 */
public class ChiTietLoTrinh {

    ChiTietLoTrinhDAO chiTietLoTrinhDao = new ChiTietLoTrinhDAO();

    public static ArrayList<ChiTietLoTrinhObj> getListChiTietLoTrinh(String MaNhanVien, String NgayPhanCa) {
        System.out.println("-------------lay danh sach chi tiet lo trinh------------");
        ResultSet rs = ChiTietLoTrinhDAO.getChiTietLoTrinh(MaNhanVien, NgayPhanCa);
        ArrayList<ChiTietLoTrinhObj> arrChiTietLoTrinh = new ArrayList<ChiTietLoTrinhObj>();
        //get list chi tiet lo trinh
        ChiTietLoTrinhObj obj;
        if (rs != null) {
            try {
                while (rs.next()) {
                    obj = new ChiTietLoTrinhObj();
                    obj.setMaLoTrinh(rs.getInt("t_malotrinh"));
                    obj.setTenLoTrinh(rs.getString("t_tenlotrinh"));
                    obj.setTenDiem(rs.getString("t_tendiadiem"));
                    obj.setNgayPhanCa(rs.getString("t_ngayphanca"));
                    if (rs.getString("t_noidungdanhgia") == null) {
                        obj.setDanhGia("");

                    } else {
                        obj.setDanhGia(rs.getString("t_noidungdanhgia"));
                    }
                    obj.setCameraTruoc(rs.getInt("t_cameratruoc"));
                    obj.setCameraSau(rs.getInt("t_camerasau"));
                    obj.setThoiGianBD(rs.getString("t_thoigianbd"));
                    obj.setThuTu(rs.getInt("t_thutu"));
                    obj.setKiemSoatThoiGian(rs.getInt("t_kstg"));
                    obj.setThoiGianThucHien(rs.getInt("t_thoigianthuchien"));
                    obj.setRFIDDD(rs.getString("t_rfid"));
                    obj.setMaBoPhan(rs.getInt("t_mabophan"));
                    obj.setMaDiem(rs.getInt("t_madiem"));
                    obj.setMaDiem2(rs.getInt("t_madiem2"));
                    obj.setMaNhanVien(rs.getInt("t_manhanvien"));
                    obj.setThoiGianNghi(rs.getInt("t_thoigiannghi"));
                    obj.setThoiGianLoTrinh(rs.getInt("t_thoigianlotrinh"));
                    obj.setVong(rs.getInt("t_sovong"));
                    obj.setThuTuLoTrinh(rs.getInt("t_thutulotrinh"));// thêm ngày 7/9/2016
                    obj.setMaPhanCong(rs.getInt("t_maphancong"));
                    obj.setMoTa(rs.getString("t_motadiem"));
                    //get Field in oject
                    arrChiTietLoTrinh.add(obj);
                    //add ojects in arrlist()
                }
                System.out.println("/////Lay danh sach chi tiet lo trinh thanh cong//////");
                return arrChiTietLoTrinh;
            } catch (SQLException ex) {
                ex.printStackTrace();

                System.out.println("//////Error in process/////");
            }
            //
        }
        System.out.println("/////Error Null//////");
        return null;
    }
}
