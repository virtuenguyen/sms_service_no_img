/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.db;

import com.webservice.DAO.CongViecTaiMotDiemDAO;
import com.webservice.objects.CongViecTaiMotDiemObj;
import java.sql.ResultSet;

/**
 *
 * @author TIEN
 */
public class CongViecTaiMotDiem {

    CongViecTaiMotDiemDAO congViecTaiMotDiemDAO = new CongViecTaiMotDiemDAO();

    public static CongViecTaiMotDiemObj getCongViecTaiMotDiem(String mnv, int mlt, int mdd) {
        CongViecTaiMotDiemObj obj = new CongViecTaiMotDiemObj();
//        Timestamp hientai = new Timestamp(new Date().getTime());
        ResultSet rsllv = CongViecTaiMotDiemDAO.getCongViecTaiMotDiem(mnv, mlt, mdd);
        if (rsllv != null) {
            try {
                if (rsllv.first()) {
                    obj.setCameraTruoc(rsllv.getInt("CameraTruoc"));
                    obj.setCameraSau(rsllv.getInt("CameraSau"));
                    obj.setCVDanhGia(rsllv.getString("CVDanhGia"));
                    return obj;
                }
                System.out.println("/////Dia diem nay k co cong viec//////");
                return null;
            } catch (Exception e) {
                System.out.println("//////Loi trong qua trinh xu ly/////");
            }
            //
        }
        System.out.println("/////Error//////");
        return null;
    }
}
