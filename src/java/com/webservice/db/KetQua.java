/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.db;

import com.webservice.DAO.DanhGiaDiemDAO;
import com.webservice.DAO.KetQuaChupHinhDAO;
import com.webservice.DAO.KetQuaDiemDAO;
import com.webservice.DAO.KetQuaLoTrinhDAO;
import com.webservice.DAO.KetQuaThucHienDAO;
import com.webservice.objects.DTO_AN_ChupAnh;
import com.webservice.objects.DTO_AN_DanhGia;
import com.webservice.objects.DTO_AN_Diem;
import com.webservice.objects.DTO_AN_LoTrinh;
import com.webservice.objects.DanhGiaDiemObj;
import com.webservice.objects.KetQuaChupHinhObj;
import com.webservice.objects.KetQuaThucHienObj;
import com.webservice.objects.LoaiHinhObj;
import com.webservice.utilities.ClassConvertData;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author TIEN
 */
public class KetQua {

    KetQuaLoTrinhDAO dao = new KetQuaLoTrinhDAO();
    KetQuaDiemDAO daoDiem = new KetQuaDiemDAO();

    public String insert(String ja) {
//        System.out.println("Param la :" + param);

        try {
            JSONObject ketQuaObj = new JSONObject(ja);

            JSONArray chupAnhArrayList = ketQuaObj.getJSONArray("chupAnhArrayList");

            JSONArray danhGiaArrayList = ketQuaObj.getJSONArray("danhGiaArrayList");

            JSONObject ketQuaLTDD = ketQuaObj.getJSONObject("loTrinh_dto");

            String thoigianquetthethucte = ketQuaObj.getString("thoigianquetthethucte");
            String thoigianyeucauquetthe = ketQuaObj.getString("thoigianyeucauquetthe");
            int malotrinh = ketQuaObj.getInt("malotrinh");
            String tendiem = ketQuaObj.getString("tendiem");
            int status = ketQuaObj.getInt("status");
            int thutu = ketQuaObj.getInt("thutudiem");
            String kiemsoattg = ketQuaObj.getString("kiemsoatthoigian");
//            JSONObject objLoTrinh =ketQuaObj.getJSONObject("loTrinh_dto");

            System.out.println("ket qua" + thoigianquetthethucte + malotrinh + tendiem + status);
            System.out.println("chupanh_array :" + chupAnhArrayList);
            System.out.println("danhgia_array:" + danhGiaArrayList);
            System.out.println("LoTrinh la:" + ketQuaLTDD.toString());
            System.out.println("Kết quả là:" + ketQuaObj);
//        Khởi tạo 4 ojb trên
            DTO_AN_LoTrinh ketQuaLT = new DTO_AN_LoTrinh();
            DTO_AN_Diem ketQuaDiem = new DTO_AN_Diem();
            DTO_AN_ChupAnh ketQuaCH;
            DTO_AN_DanhGia danhGia;

            //insert kết quả lộ trình
            ketQuaLT.setMa_lo_trinh(ketQuaLTDD.getInt("malotrinh"));
            ketQuaLT.setMa_nhan_vien(ketQuaLTDD.getInt("manhanvien"));
            ketQuaLT.setNgay_phan_ca(ketQuaLTDD.getString("ngayphanca"));

            int maPC = 0;
            ResultSet rsMaPC = dao.searchMaPhanCong(ketQuaLTDD.getInt("manhanvien"), ketQuaLTDD.getString("ngayphanca"));
            if (rsMaPC != null && rsMaPC.isBeforeFirst()) {
                rsMaPC.first();
                maPC = rsMaPC.getInt(1);
            }
            ketQuaLT.setMa_phan_cong(maPC);
            ketQuaLT.setSo_diem(ketQuaLTDD.getInt("sodiem"));
            ketQuaLT.setTen_lo_trinh(ketQuaLTDD.getString("tenlotrinh"));
            ketQuaLT.setThoi_gian_bat_dau(ketQuaLTDD.getString("thoigianbatdau"));
            ketQuaLT.setThoi_gian_lo_trinh(ketQuaLTDD.getInt("thoigianlotrinh"));
            ketQuaLT.setThoi_gian_nghi(ketQuaLTDD.getInt("thoigiannghi"));
            ketQuaLT.setTrang_thai_danh_gia_lo_trinh(ketQuaLTDD.getInt("trangthaidanhgialotrinh"));
            ketQuaLT.setVong(ketQuaLTDD.getInt("vonghientai"));
            ketQuaLT.setThu_tu_lo_trinh(ketQuaLTDD.getInt("thutu_lotrinh"));

            int idLoTrinh = 0;
            ResultSet rs = dao.insert_DAO_AN_LoTrinh(ketQuaLT);
            if (rs != null && rs.isBeforeFirst()) {
                rs.first();
                idLoTrinh = rs.getInt(1);
            }

            ketQuaDiem.setKiem_soat_thoi_gian(kiemsoattg);
            ketQuaDiem.setTen_diem(tendiem);
            ketQuaDiem.setThoi_gian_quyet_thuc_te(thoigianquetthethucte);
            ketQuaDiem.setThu_tu(thutu);
            ketQuaDiem.setMa_lo_trinh(idLoTrinh);
            ketQuaDiem.setTrang_thai_nhan_vien(status);
            ketQuaDiem.setThoi_gian_thuc_hien(ketQuaObj.getInt("thoigian_thuchien"));
            ketQuaDiem.setThoi_gian_yeu_cau_quyet_the(thoigianyeucauquetthe);
            ketQuaDiem.setTrang_thai_danh_gia(ketQuaObj.getInt("trangthaidanhgia"));
            int idDiem = 0;
            ResultSet rsDiem = daoDiem.insert_DAO_AN_Diem(ketQuaDiem);
            if (rsDiem != null && rsDiem.isBeforeFirst()) {
                rsDiem.first();
                idDiem = rsDiem.getInt(1);
            }

            // Đánh giá
            ArrayList<DTO_AN_DanhGia> arrDanhGia = new ArrayList<DTO_AN_DanhGia>();
            for (int i1 = 0; i1 < danhGiaArrayList.length(); i1++) {
                danhGia = new DTO_AN_DanhGia();
                JSONObject danhGiaObj = danhGiaArrayList.getJSONObject(i1);
//                danhGia.setId_danh_gia(danhGiaObj.getInt("id_danhgia"));
                danhGia.setId_diem(idDiem);
                if(danhGiaObj.has("file")){
                    
                danhGia.setAnh_chup(ClassConvertData.ConvertStringToImage(danhGiaObj.getString("file")));
                }
                else{
                    danhGia.setAnh_chup(null);
                }
                danhGia.setNoi_dung_danh_gia(danhGiaObj.getString("noidung_danhgia"));
                danhGia.setKet_qua_danh_gia(danhGiaObj.getString("kqdanhgia"));
                if (danhGiaObj.has("chuthich")) {
                    danhGia.setChu_thich(danhGiaObj.getString("chuthich"));
                } else{
                    // nếu không có chú thích
                    danhGia.setChu_thich("");
                }

                arrDanhGia.add(danhGia);
            }
            DanhGiaDiemDAO.insertDanhGiaDiaDiem(arrDanhGia);
            System.out.println("js arr_danhGia: " + danhGiaArrayList.toString());

            //chụp hình
            ArrayList<DTO_AN_ChupAnh> arrChupHinh = new ArrayList<DTO_AN_ChupAnh>();
            for (int j = 0; j < chupAnhArrayList.length(); j++) {
                ketQuaCH = new DTO_AN_ChupAnh();
                JSONObject chupHinh = chupAnhArrayList.getJSONObject(j);// lấy kết quả từng obj chụp hinh của điểm từ Android
                int loaiHinh = chupHinh.getInt("loai_anh");
                if (loaiHinh == 2) {
                    ketQuaCH.setCamera_sau(ClassConvertData.ConvertStringToImage(chupHinh.getString("file_anh")));
                } else if (loaiHinh == 1) {
                    ketQuaCH.setCamera_truoc(ClassConvertData.ConvertStringToImage(chupHinh.getString("file_anh")));
                }
                ketQuaCH.setId_Diem(idDiem);
                arrChupHinh.add(ketQuaCH);
            }
            KetQuaChupHinhDAO.insertKetQuaChupHinh(arrChupHinh);

            System.out.println("js arr_ChupHinh: " + chupAnhArrayList.toString());
            return "ok";
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "no";
    }

}
