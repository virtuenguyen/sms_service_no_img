/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.db;

import com.webservice.DAO.LichLamViecDAO;
import com.webservice.LichCongViecConvert.LichCongViecConvert;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author TIEN
 */
public class LichCongViec {
    
    LichLamViecDAO lichLamViecDAO = new LichLamViecDAO();
    
    public static ArrayList<LichCongViecConvert> CheckLichLamViec(String mnv, String thang) {
        ArrayList<LichCongViecConvert> arr = new ArrayList<LichCongViecConvert>();
//        Timestamp hientai = new Timestamp(new Date().getTime());
        ResultSet rsllv = LichLamViecDAO.checkLichLamViec(mnv, thang);
        LichCongViecConvert obj;
        if (rsllv != null) {
            try {
                while (rsllv.next()) {
                    obj = new LichCongViecConvert();
                    obj.setMaNhanVien(rsllv.getString("ma_nhan_vien"));
                    obj.setMaLoTrinh(rsllv.getInt("ma_lo_trinh"));
                    obj.setNgayPhanCa(rsllv.getString("ngay_phan_ca"));
                    
                    arr.add(obj);
                }
                System.out.println("/////Lấy danh sách lịch làm việc thành công//////");
                return arr;
            } catch (SQLException ex) {
                ex.printStackTrace();
                System.out.println("//////Vòng while lịch công việc có lỗi/////");
            }
            //
        }
        System.out.println("/////rs null//////");
        return null;
    }
}
