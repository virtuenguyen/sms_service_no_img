/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.db;

import com.webservice.DAO.NhanVienDAO;
import com.webservice.objects.NhanVienObj;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author TIEN
 */
public class NhanVien {

    NhanVienDAO nhanVienDAO = new NhanVienDAO();

    public static NhanVienObj checkLoginV2(String id, String pass) {
        NhanVienObj obj = new NhanVienObj();
//        Timestamp TimeServer = new Timestamp(new Date().getTime());
        ResultSet rs = NhanVienDAO.checkLoginNV(id, pass);
        if (rs != null) {
            try {
                if (rs.first()) {
                    System.out.println("/////Đăng nhập thành công//////");
                    obj.setRFID(rs.getString("RFID").toUpperCase());
                    obj.setMaNhanVien(rs.getString("ma_nhan_vien"));
                    obj.setTenNhanVien(rs.getString("ten_nhan_vien"));
                    obj.setDiaChi(rs.getString("dia_chi"));
                    obj.setMaBoPhan(rs.getInt("ma_bo_phan"));
                    obj.setDienThoai(rs.getString("sdt"));
//                    String ha = org.apache.commons.codec.binary.Base64.encodeBase64String(rs.getBytes("HinhAnh"));
//                    obj.setHinhAnh(ha);
                    obj.setTenDangNhap(rs.getString("ten_dang_nhap"));
                    obj.setMatKhau(rs.getString("mat_khau"));
                    obj.setTenBoPhan(rs.getString("ten_bo_phan"));
//                    String matKhau = encryptionString.decode(rs.getString("MatKhau"));
//                    System.out.println("-----------insert lich su dang nhap--------");
//                    LichSuLoginDAO.insert(obj.getMaNhanVien(), TimeServer, TGDangNhap, hinhanh);
                    return obj;
                }
            } catch (SQLException ex) {
                System.out.println("----------loi kiem tra dang nhap----------");
            }
        }
        System.out.println("/////Dang nhap that bai//////");
        return null;
    }

    public static boolean ChangePassword(String ma_nhan_vien, String mat_khau) {
        int manv = 0;
        if (!ma_nhan_vien.isEmpty()) {
            manv = Integer.parseInt(ma_nhan_vien);
        }
        if (NhanVienDAO.changePass(manv, mat_khau)) {
            System.out.println("/////Doi mat khau thanh cong//////");
            return true;
        } else {
            System.out.println("/////Doi mat khau that bai////");
            return false;
        }
    }

//    public static NhanVienObj checkLogin(String id, String pass) {
//        NhanVienObj obj = new NhanVienObj();
//        ResultSet rs = NhanVienDAO.checkLoginNV(id, pass);
//        if (rs != null) {
//            try {
//                if (rs.first()) {
//                    System.out.println("/////Dang nhap thanh cong//////");
//                    obj.setMaNhanVien(rs.getString("MaNhanVien"));
//                    obj.setDiaChi(rs.getString("DiaChi"));
//                    obj.setDienThoai(rs.getString("DienThoai"));
//                    String ha = org.apache.commons.codec.binary.Base64.encodeBase64String(rs.getBytes("HinhAnh"));
//                    obj.setHinhAnh(ha);
//                    obj.setMaBoPhan(rs.getInt("mabophan"));
//                    obj.setRFID(rs.getString("RFID"));
//                    return obj;
//                }
//            } catch (SQLException ex) {
//                System.out.println("----------loi kiem tra dang nhap----------");
//            }
//        }
//        System.out.println("/////Dang nhap that bai//////");
//        return null;
//    }

}
