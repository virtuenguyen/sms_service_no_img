/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TIEN
 */
@XmlRootElement
//500 (Internal Server Error) loi do
public class ChiTietLoTrinhObj {
    int MaLoTrinh, CameraTruoc, CameraSau, KiemSoatThoiGian, ThuTu, ThoiGianThucHien, MaNhanVien, MaDiem, MaDiem2, MaBoPhan, ThoiGianLoTrinh, ThoiGianNghi, Vong, ThuTuLoTrinh, MaPhanCong;
    String TenLoTrinh, MoTa;
    String TenDiem;
    String NgayPhanCa;
    String RFIDDD;
    String ThoiGianBD;
    String DanhGia;

    public String getRFIDDD() {
        return RFIDDD;
    }

    public int getThuTuLoTrinh() {
        return ThuTuLoTrinh;
    }

    public void setThuTuLoTrinh(int ThuTuLoTrinh) {
        this.ThuTuLoTrinh = ThuTuLoTrinh;
    }

    public int getMaPhanCong() {
        return MaPhanCong;
    }

    public void setMaPhanCong(int MaPhanCong) {
        this.MaPhanCong = MaPhanCong;
    }

    public String getMoTa() {
        return MoTa;
    }

    public void setMoTa(String MoTa) {
        this.MoTa = MoTa;
    }

    public int getVong() {
        return Vong;
    }

    public void setVong(int Vong) {
        this.Vong = Vong;
    }

    public int getMaNhanVien() {
        return MaNhanVien;
    }

    public void setMaNhanVien(int MaNhanVien) {
        this.MaNhanVien = MaNhanVien;
    }

    public int getMaDiem() {
        return MaDiem;
    }

    public void setMaDiem(int MaDiem) {
        this.MaDiem = MaDiem;
    }

    public int getMaDiem2() {
        return MaDiem2;
    }

    public void setMaDiem2(int MaDiem2) {
        this.MaDiem2 = MaDiem2;
    }

    public int getMaBoPhan() {
        return MaBoPhan;
    }

    public void setMaBoPhan(int MaBoPhan) {
        this.MaBoPhan = MaBoPhan;
    }

    public int getThoiGianLoTrinh() {
        return ThoiGianLoTrinh;
    }

    public void setThoiGianLoTrinh(int ThoiGianLoTrinh) {
        this.ThoiGianLoTrinh = ThoiGianLoTrinh;
    }

    public int getThoiGianNghi() {
        return ThoiGianNghi;
    }

    public void setThoiGianNghi(int ThoiGianNghi) {
        this.ThoiGianNghi = ThoiGianNghi;
    }

    public void setRFIDDD(String RFIDDD) {
        this.RFIDDD = RFIDDD;
    }

    public int getKiemSoatThoiGian() {
        return KiemSoatThoiGian;
    }

    public void setKiemSoatThoiGian(int KiemSoatThoiGian) {
        this.KiemSoatThoiGian = KiemSoatThoiGian;
    }

    public int getThuTu() {
        return ThuTu;
    }

    public void setThuTu(int ThuTu) {
        this.ThuTu = ThuTu;
    }

    public int getThoiGianThucHien() {
        return ThoiGianThucHien;
    }

    public void setThoiGianThucHien(int ThoiGianThucHien) {
        this.ThoiGianThucHien = ThoiGianThucHien;
    }

    public ChiTietLoTrinhObj() {
    }

    public int getMaLoTrinh() {
        return MaLoTrinh;
    }

    public void setMaLoTrinh(int MaLoTrinh) {
        this.MaLoTrinh = MaLoTrinh;
    }

    public int getCameraTruoc() {
        return CameraTruoc;
    }

    public void setCameraTruoc(int CameraTruoc) {
        this.CameraTruoc = CameraTruoc;
    }

    public int getCameraSau() {
        return CameraSau;
    }

    public void setCameraSau(int CameraSau) {
        this.CameraSau = CameraSau;
    }

    public String getTenLoTrinh() {
        return TenLoTrinh;
    }

    public void setTenLoTrinh(String TenLoTrinh) {
        this.TenLoTrinh = TenLoTrinh;
    }

    public String getTenDiem() {
        return TenDiem;
    }

    public void setTenDiem(String TenDiem) {
        this.TenDiem = TenDiem;
    }

    public String getNgayPhanCa() {
        return NgayPhanCa;
    }

    public void setNgayPhanCa(String NgayPhanCa) {
        this.NgayPhanCa = NgayPhanCa;
    }

    public String getThoiGianBD() {
        return ThoiGianBD;
    }

    public void setThoiGianBD(String time) {
        this.ThoiGianBD = time;
    }

    public String getDanhGia() {
        return DanhGia;
    }

    public void setDanhGia(String DanhGia) {
        this.DanhGia = DanhGia;
    }
   
}
