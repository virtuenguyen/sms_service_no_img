/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TIEN
 */
@XmlRootElement
public class CongViecTaiMotDiemObj {
    int CameraTruoc;
    int CameraSau;
    String CVDanhGia;

    public CongViecTaiMotDiemObj() {
    }

    public int getCameraTruoc() {
        return CameraTruoc;
    }

    public void setCameraTruoc(int CameraTruoc) {
        this.CameraTruoc = CameraTruoc;
    }

    public int getCameraSau() {
        return CameraSau;
    }

    public void setCameraSau(int CameraSau) {
        this.CameraSau = CameraSau;
    }

    public String getCVDanhGia() {
        return CVDanhGia;
    }

    public void setCVDanhGia(String CVDanhGia) {
        this.CVDanhGia = CVDanhGia;
    }
    
}
