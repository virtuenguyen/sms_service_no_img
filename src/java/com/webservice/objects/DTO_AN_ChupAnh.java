/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author Hien
 */
public class DTO_AN_ChupAnh {

    private int ma_chup_anh, id_Diem, loai_hinh;
    private byte[] camera_truoc, camera_sau;

    public DTO_AN_ChupAnh() {
    }

    public DTO_AN_ChupAnh(int ma_chup_anh, int id_Diem, int loai_hinh, byte[] camera_truoc, byte[] camera_sau) {
        this.ma_chup_anh = ma_chup_anh;
        this.id_Diem = id_Diem;
        this.loai_hinh = loai_hinh;
        this.camera_truoc = camera_truoc;
        this.camera_sau = camera_sau;
    }

    public int getLoai_hinh() {
        return loai_hinh;
    }

    public void setLoai_hinh(int loai_hinh) {
        this.loai_hinh = loai_hinh;
    }

    public int getMa_chup_anh() {
        return ma_chup_anh;
    }

    public void setMa_chup_anh(int ma_chup_anh) {
        this.ma_chup_anh = ma_chup_anh;
    }

    public int getId_Diem() {
        return id_Diem;
    }

    public void setId_Diem(int id_Diem) {
        this.id_Diem = id_Diem;
    }

    public byte[] getCamera_truoc() {
        return camera_truoc;
    }

    public void setCamera_truoc(byte[] camera_truoc) {
        this.camera_truoc = camera_truoc;
    }

    public byte[] getCamera_sau() {
        return camera_sau;
    }

    public void setCamera_sau(byte[] camera_sau) {
        this.camera_sau = camera_sau;
    }

}
