/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

import java.sql.Blob;

/**
 *
 * @author Hien
 */
public class DTO_AN_DanhGia {

    private int id_danh_gia, id_diem;
    private String noi_dung_danh_gia, ket_qua_danh_gia, chu_thich;
    private byte[] anh_chup;

    public DTO_AN_DanhGia() {
    }

    public int getId_danh_gia() {
        return id_danh_gia;
    }

    public void setId_danh_gia(int id_danh_gia) {
        this.id_danh_gia = id_danh_gia;
    }

    public int getId_diem() {
        return id_diem;
    }

    public void setId_diem(int id_diem) {
        this.id_diem = id_diem;
    }

    public String getNoi_dung_danh_gia() {
        return noi_dung_danh_gia;
    }

    public void setNoi_dung_danh_gia(String noi_dung_danh_gia) {
        this.noi_dung_danh_gia = noi_dung_danh_gia;
    }

    public byte[] getAnh_chup() {
        return anh_chup;
    }

    public void setAnh_chup(byte[] anh_chup) {
        this.anh_chup = anh_chup;
    }


    public String getKet_qua_danh_gia() {
        return ket_qua_danh_gia;
    }

    public void setKet_qua_danh_gia(String ket_qua_danh_gia) {
        this.ket_qua_danh_gia = ket_qua_danh_gia;
    }

    public String getChu_thich() {
        return chu_thich;
    }

    public void setChu_thich(String chu_thich) {
        this.chu_thich = chu_thich;
    }

}
