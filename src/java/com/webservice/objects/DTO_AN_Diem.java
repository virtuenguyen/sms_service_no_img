/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author Hien
 */
public class DTO_AN_Diem {

    private int id, thu_tu, thoi_gian_thuc_hien, ma_lo_trinh, ma_diem1, ma_diem2, trang_thai_nhan_vien, ma_phan_cong, trang_thai_danh_gia;
    private String ten_diem, kiem_soat_thoi_gian, thoi_gian_yeu_cau_quyet_the, thoi_gian_quyet_thuc_te, rfid, camera_truoc, camera_sau;
    private boolean trang_thai;

    public DTO_AN_Diem() {
    }

    public DTO_AN_Diem(int id, int thu_tu, int thoi_gian_thuc_hien, int ma_lo_trinh, int ma_diem1, int ma_diem2, int trang_thai_nhan_vien, int ma_phan_cong, int trang_thai_danh_gia, String ten_diem, String kiem_soat_thoi_gian, String thoi_gian_yeu_cau_quyet_the, String thoi_gian_quyet_thuc_te, String rfid, String camera_truoc, String camera_sau, boolean trang_thai) {
        this.id = id;
        this.thu_tu = thu_tu;
        this.thoi_gian_thuc_hien = thoi_gian_thuc_hien;
        this.ma_lo_trinh = ma_lo_trinh;
        this.ma_diem1 = ma_diem1;
        this.ma_diem2 = ma_diem2;
        this.trang_thai_nhan_vien = trang_thai_nhan_vien;
        this.ma_phan_cong = ma_phan_cong;
        this.trang_thai_danh_gia = trang_thai_danh_gia;
        this.ten_diem = ten_diem;
        this.kiem_soat_thoi_gian = kiem_soat_thoi_gian;
        this.thoi_gian_yeu_cau_quyet_the = thoi_gian_yeu_cau_quyet_the;
        this.thoi_gian_quyet_thuc_te = thoi_gian_quyet_thuc_te;
        this.rfid = rfid;
        this.camera_truoc = camera_truoc;
        this.camera_sau = camera_sau;
        this.trang_thai = trang_thai;
    }

    public int getTrang_thai_danh_gia() {
        return trang_thai_danh_gia;
    }

    public void setTrang_thai_danh_gia(int trang_thai_danh_gia) {
        this.trang_thai_danh_gia = trang_thai_danh_gia;
    }

    public int getMa_phan_cong() {
        return ma_phan_cong;
    }

    public void setMa_phan_cong(int ma_phan_cong) {
        this.ma_phan_cong = ma_phan_cong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getThu_tu() {
        return thu_tu;
    }

    public void setThu_tu(int thu_tu) {
        this.thu_tu = thu_tu;
    }

    public int getThoi_gian_thuc_hien() {
        return thoi_gian_thuc_hien;
    }

    public void setThoi_gian_thuc_hien(int thoi_gian_thuc_hien) {
        this.thoi_gian_thuc_hien = thoi_gian_thuc_hien;
    }

    public int getMa_lo_trinh() {
        return ma_lo_trinh;
    }

    public void setMa_lo_trinh(int ma_lo_trinh) {
        this.ma_lo_trinh = ma_lo_trinh;
    }

    public int getMa_diem1() {
        return ma_diem1;
    }

    public void setMa_diem1(int ma_diem1) {
        this.ma_diem1 = ma_diem1;
    }

    public int getMa_diem2() {
        return ma_diem2;
    }

    public void setMa_diem2(int ma_diem2) {
        this.ma_diem2 = ma_diem2;
    }

    public int getTrang_thai_nhan_vien() {
        return trang_thai_nhan_vien;
    }

    public void setTrang_thai_nhan_vien(int trang_thai_danh_gia) {
        this.trang_thai_nhan_vien = trang_thai_danh_gia;
    }

    public String getTen_diem() {
        return ten_diem;
    }

    public void setTen_diem(String ten_diem) {
        this.ten_diem = ten_diem;
    }

    public String getKiem_soat_thoi_gian() {
        return kiem_soat_thoi_gian;
    }

    public void setKiem_soat_thoi_gian(String kiem_soat_thoi_gian) {
        this.kiem_soat_thoi_gian = kiem_soat_thoi_gian;
    }

    public String getThoi_gian_yeu_cau_quyet_the() {
        return thoi_gian_yeu_cau_quyet_the;
    }

    public void setThoi_gian_yeu_cau_quyet_the(String thoi_gian_yeu_cau_quyet_the) {
        this.thoi_gian_yeu_cau_quyet_the = thoi_gian_yeu_cau_quyet_the;
    }

    public String getThoi_gian_quyet_thuc_te() {
        return thoi_gian_quyet_thuc_te;
    }

    public void setThoi_gian_quyet_thuc_te(String thoi_gian_quyet_thuc_te) {
        this.thoi_gian_quyet_thuc_te = thoi_gian_quyet_thuc_te;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public String getCamera_truoc() {
        return camera_truoc;
    }

    public void setCamera_truoc(String camera_truoc) {
        this.camera_truoc = camera_truoc;
    }

    public String getCamera_sau() {
        return camera_sau;
    }

    public void setCamera_sau(String camera_sau) {
        this.camera_sau = camera_sau;
    }

    public boolean isTrang_thai() {
        return trang_thai;
    }

    public void setTrang_thai(boolean trang_thai) {
        this.trang_thai = trang_thai;
    }

}
