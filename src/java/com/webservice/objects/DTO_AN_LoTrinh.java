/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author Hien
 */
public class DTO_AN_LoTrinh {

    private int ma_lo_trinh, ma_nhan_vien, so_diem, trang_thai_danh_gia_lo_trinh, thoi_gian_lo_trinh, thoi_gian_nghi, ma_phan_cong, vong, thu_tu_lo_trinh;
    private String ten_lo_trinh, ngay_phan_ca, thoi_gian_bat_dau;
    private boolean status_upload;

    public DTO_AN_LoTrinh() {
    }

    public DTO_AN_LoTrinh(int ma_lo_trinh, int ma_nhan_vien, int so_diem, int vong, int trang_thai_danh_gia_lo_trinh, int thoi_gian_lo_trinh, int thoi_gian_nghi, int ma_phan_cong, String ten_lo_trinh, String ngay_phan_ca, String thoi_gian_bat_dau, boolean status_upload) {
        this.ma_lo_trinh = ma_lo_trinh;
        this.ma_nhan_vien = ma_nhan_vien;
        this.so_diem = so_diem;
        this.trang_thai_danh_gia_lo_trinh = trang_thai_danh_gia_lo_trinh;
        this.thoi_gian_lo_trinh = thoi_gian_lo_trinh;
        this.thoi_gian_nghi = thoi_gian_nghi;
        this.ma_phan_cong = ma_phan_cong;
        this.ten_lo_trinh = ten_lo_trinh;
        this.ngay_phan_ca = ngay_phan_ca;
        this.thoi_gian_bat_dau = thoi_gian_bat_dau;
        this.status_upload = status_upload;
        this.vong = vong;
    }

    public int getThu_tu_lo_trinh() {
        return thu_tu_lo_trinh;
    }

    public void setThu_tu_lo_trinh(int thu_tu_lo_trinh) {
        this.thu_tu_lo_trinh = thu_tu_lo_trinh;
    }

    public int getVong() {
        return vong;
    }

    public void setVong(int vong) {
        this.vong = vong;
    }

    public String getThoi_gian_bat_dau() {
        return thoi_gian_bat_dau;
    }

    public void setThoi_gian_bat_dau(String thoi_gian_bat_dau) {
        this.thoi_gian_bat_dau = thoi_gian_bat_dau;
    }

    public int getMa_lo_trinh() {
        return ma_lo_trinh;
    }

    public void setMa_lo_trinh(int ma_lo_trinh) {
        this.ma_lo_trinh = ma_lo_trinh;
    }

    public int getMa_nhan_vien() {
        return ma_nhan_vien;
    }

    public void setMa_nhan_vien(int ma_nhan_vien) {
        this.ma_nhan_vien = ma_nhan_vien;
    }

    public int getMa_phan_cong() {
        return ma_phan_cong;
    }

    public void setMa_phan_cong(int ma_phan_cong) {
        this.ma_phan_cong = ma_phan_cong;
    }

    public int getSo_diem() {
        return so_diem;
    }

    public void setSo_diem(int so_diem) {
        this.so_diem = so_diem;
    }

    public int getTrang_thai_danh_gia_lo_trinh() {
        return trang_thai_danh_gia_lo_trinh;
    }

    public void setTrang_thai_danh_gia_lo_trinh(int trang_thai_danh_gia_lo_trinh) {
        this.trang_thai_danh_gia_lo_trinh = trang_thai_danh_gia_lo_trinh;
    }

    public int getThoi_gian_lo_trinh() {
        return thoi_gian_lo_trinh;
    }

    public void setThoi_gian_lo_trinh(int thoi_gian_lo_trinh) {
        this.thoi_gian_lo_trinh = thoi_gian_lo_trinh;
    }

    public int getThoi_gian_nghi() {
        return thoi_gian_nghi;
    }

    public void setThoi_gian_nghi(int thoi_gian_nghi) {
        this.thoi_gian_nghi = thoi_gian_nghi;
    }

    public String getTen_lo_trinh() {
        return ten_lo_trinh;
    }

    public void setTen_lo_trinh(String ten_lo_trinh) {
        this.ten_lo_trinh = ten_lo_trinh;
    }

    public String getNgay_phan_ca() {
        return ngay_phan_ca;
    }

    public void setNgay_phan_ca(String ngay_phan_ca) {
        this.ngay_phan_ca = ngay_phan_ca;
    }

    public boolean isStatus_upload() {
        return status_upload;
    }

    public void setStatus_upload(boolean status_upload) {
        this.status_upload = status_upload;
    }

}
