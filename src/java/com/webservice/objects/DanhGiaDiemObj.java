/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author TIEN
 */
public class DanhGiaDiemObj {
    private int ma_danh_gia, ma_kq_thuc_hien;
    private String trang_thai_diem, url_hinh_anh;

    public DanhGiaDiemObj() {
    }

    public int getMa_danh_gia() {
        return ma_danh_gia;
    }

    public void setMa_danh_gia(int ma_danh_gia) {
        this.ma_danh_gia = ma_danh_gia;
    }

    public int getMa_kq_thuc_hien() {
        return ma_kq_thuc_hien;
    }

    public void setMa_kq_thuc_hien(int ma_kq_thuc_hien) {
        this.ma_kq_thuc_hien = ma_kq_thuc_hien;
    }

    public String getTrang_thai_diem() {
        return trang_thai_diem;
    }

    public void setTrang_thai_diem(String trang_thai_diem) {
        this.trang_thai_diem = trang_thai_diem;
    }

    public String getUrl_hinh_anh() {
        return url_hinh_anh;
    }

    public void setUrl_hinh_anh(String url_hinh_anh) {
        this.url_hinh_anh = url_hinh_anh;
    }

    @Override
    public String toString() {
        return "DanhGiaDiemObj{" + "ma_danh_gia=" + ma_danh_gia + ", ma_kq_thuc_hien=" + ma_kq_thuc_hien + ", trang_thai_diem=" + trang_thai_diem + ", url_hinh_anh=" + url_hinh_anh + '}';
    }
    
    
}
