/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author TIEN
 */
public class KetQuaChupHinhObj {
    private int ma_chup_hinh, ma_kq_thuc_hien, ma_loai_hinh;
    private String url_hinh_chup;

    public KetQuaChupHinhObj() {
    }

    public int getMa_chup_hinh() {
        return ma_chup_hinh;
    }

    public void setMa_chup_hinh(int ma_chup_hinh) {
        this.ma_chup_hinh = ma_chup_hinh;
    }

    public int getMa_kq_thuc_hien() {
        return ma_kq_thuc_hien;
    }

    public void setMa_kq_thuc_hien(int ma_kq_thuc_hien) {
        this.ma_kq_thuc_hien = ma_kq_thuc_hien;
    }

    public int getMa_loai_hinh() {
        return ma_loai_hinh;
    }

    public void setMa_loai_hinh(int ma_loai_hinh) {
        this.ma_loai_hinh = ma_loai_hinh;
    }

    public String getUrl_hinh_chup() {
        return url_hinh_chup;
    }

    public void setUrl_hinh_chup(String url_hinh_chup) {
        this.url_hinh_chup = url_hinh_chup;
    }

    @Override
    public String toString() {
        return "KetQuaChupHinhObj{" + "ma_chup_hinh=" + ma_chup_hinh + ", ma_kq_thuc_hien=" + ma_kq_thuc_hien + ", ma_loai_hinh=" + ma_loai_hinh + ", url_hinh_chup=" + url_hinh_chup + '}';
    }
    
}
