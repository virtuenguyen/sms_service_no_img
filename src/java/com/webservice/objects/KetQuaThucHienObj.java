/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author TIEN
 */
public class KetQuaThucHienObj {
    private int ma_kq_thuc_hien, ma_phan_cong, ma_diem;
    private String tg_yeu_cau_quet_the, tg_quet_the_thuc_te, trang_thai_quet_the;

    public KetQuaThucHienObj() {
    }

    public int getMa_kq_thuc_hien() {
        return ma_kq_thuc_hien;
    }

    public void setMa_kq_thuc_hien(int ma_kq_thuc_hien) {
        this.ma_kq_thuc_hien = ma_kq_thuc_hien;
    }

    public int getMa_phan_cong() {
        return ma_phan_cong;
    }

    public void setMa_phan_cong(int ma_phan_cong) {
        this.ma_phan_cong = ma_phan_cong;
    }

    public int getMa_diem() {
        return ma_diem;
    }

    public void setMa_diem(int ma_diem) {
        this.ma_diem = ma_diem;
    }

    public String getTg_yeu_cau_quet_the() {
        return tg_yeu_cau_quet_the;
    }

    public void setTg_yeu_cau_quet_the(String tg_yeu_cau_quet_the) {
        this.tg_yeu_cau_quet_the = tg_yeu_cau_quet_the;
    }

    public String getTg_quet_the_thuc_te() {
        return tg_quet_the_thuc_te;
    }

    public void setTg_quet_the_thuc_te(String tg_quet_the_thuc_te) {
        this.tg_quet_the_thuc_te = tg_quet_the_thuc_te;
    }

    public String getTrang_thai_quet_the() {
        return trang_thai_quet_the;
    }

    public void setTrang_thai_quet_the(String trang_thai_quet_the) {
        this.trang_thai_quet_the = trang_thai_quet_the;
    }

    @Override
    public String toString() {
        return "KetQuaThucHienObj{" + "ma_kq_thuc_hien=" + ma_kq_thuc_hien + ", ma_phan_cong=" + ma_phan_cong + ", ma_diem=" + ma_diem + ", tg_yeu_cau_quet_the=" + tg_yeu_cau_quet_the + ", tg_quet_the_thuc_te=" + tg_quet_the_thuc_te + ", trang_thai_quet_the=" + trang_thai_quet_the + '}';
    }
        
}
