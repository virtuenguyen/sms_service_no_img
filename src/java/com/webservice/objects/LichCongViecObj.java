/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author TIEN
 */
public class LichCongViecObj {
    String MaNhanVien;
    String NgayPhanCa;
    int MaLoTrinh;

    public LichCongViecObj() {
    }
    

    public String getMaNhanVien() {
        return MaNhanVien;
    }

    public void setMaNhanVien(String MaNhanVien) {
        this.MaNhanVien = MaNhanVien;
    }

    public String getNgayPhanCa() {
        return NgayPhanCa;
    }

    public void setNgayPhanCa(String NgayPhanCa) {
        this.NgayPhanCa = NgayPhanCa;
    }

    public int getMaLoTrinh() {
        return MaLoTrinh;
    }

    public void setMaLoTrinh(int MaLoTrinh) {
        this.MaLoTrinh = MaLoTrinh;
    }
    
}
