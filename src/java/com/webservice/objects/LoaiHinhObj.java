/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

/**
 *
 * @author TIEN
 */
public class LoaiHinhObj {
    private int ma_loai_hinh;
    private String ten_loai_hinh;

    public LoaiHinhObj() {
    }

    public int getMa_loai_hinh() {
        return ma_loai_hinh;
    }

    public void setMa_loai_hinh(int ma_loai_hinh) {
        this.ma_loai_hinh = ma_loai_hinh;
    }

    public String getTen_loai_hinh() {
        return ten_loai_hinh;
    }

    public void setTen_loai_hinh(String ten_loai_hinh) {
        this.ten_loai_hinh = ten_loai_hinh;
    }

    @Override
    public String toString() {
        return "LoaiHinhObj{" + "ma_loai_hinh=" + ma_loai_hinh + ", ten_loai_hinh=" + ten_loai_hinh + '}';
    }
    
    
}
