/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NhanVienObj {

    public NhanVienObj() {

    }
    private int  MaBoPhan;
    private String TenNhanVien, TenDangNhap, MatKhau, RFID, DiaChi, DienThoai, HinhAnh, MaNhanVien, TenBoPhan;

    public String getTenBoPhan() {
        return TenBoPhan;
    }

    public void setTenBoPhan(String TenBoPhan) {
        this.TenBoPhan = TenBoPhan;
    }

    public String getMaNhanVien() {
        return MaNhanVien;
    }

    public void setMaNhanVien(String MaNhanVien) {
        this.MaNhanVien = MaNhanVien;
    }

    public int getMaBoPhan() {
        return MaBoPhan;
    }

    public void setMaBoPhan(int MaBoPhan) {
        this.MaBoPhan = MaBoPhan;
    }

    public String getTenNhanVien() {
        return TenNhanVien;
    }

    public void setTenNhanVien(String TenNhanVien) {
        this.TenNhanVien = TenNhanVien;
    }

    public String getTenDangNhap() {
        return TenDangNhap;
    }

    public void setTenDangNhap(String TenDangNhap) {
        this.TenDangNhap = TenDangNhap;
    }

    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String MatKhau) {
        this.MatKhau = MatKhau;
    }

    public String getRFID() {
        return RFID;
    }

    public void setRFID(String RFID) {
        this.RFID = RFID;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getDienThoai() {
        return DienThoai;
    }

    public void setDienThoai(String DienThoai) {
        this.DienThoai = DienThoai;
    }

    public String getHinhAnh() {
        return HinhAnh;
    }

    public void setHinhAnh(String HinhAnh) {
        this.HinhAnh = HinhAnh;
    }

}
