/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.utilities;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author TIEN
 */
public class ClassConvertData {

    private final SimpleDateFormat fmTimestamp = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    private final SimpleDateFormat fmDate = new SimpleDateFormat("yyyy-MM-dd");

    public Date ConvertStringToDate(String strDate) {
        try {
            Date date = fmDate.parse(strDate);
            return date;
        } catch (ParseException ex) {
            System.out.println("-------convert date is error----------");
        }
        return null;
    }

    public Timestamp ConvertStringToTimestamp(String strTimestamp) {
        try {
            Date date = fmTimestamp.parse(strTimestamp);
            return new Timestamp(date.getTime());
        } catch (Exception e) {
            System.out.println("-------convert timestamp is error----------");
        }
        return null;
    }

    public static int ConvertStringToInt(String strInt) {
        try {
            return Integer.parseInt(strInt);
        } catch (Exception e) {
            System.out.println("-------convert int is error----------");
        }
        System.out.println("-------type for int error----------");
        return 0;
    }

    public static byte[] ConvertStringToImage(String strHinhAnh) {
        try {
            return org.apache.commons.codec.binary.Base64.decodeBase64(strHinhAnh);

        } catch (Exception e) {
            System.out.println("-------convert byte is error----------");
        }
        return null;
    }

    public static String ConvertImageToString(byte[] image) {
        try {
            return org.apache.commons.codec.binary.Base64.encodeBase64String(image);
        } catch (Exception e) {
            System.out.println("-------convert string is error----------");
        }
        return null;
    }

}
