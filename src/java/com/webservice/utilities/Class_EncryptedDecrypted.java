/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.utilities;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 *
 * @author PALT 056
 */
public class Class_EncryptedDecrypted {

    private final String key = "ezeon8547";
    private Cipher ecipher, dcipher;
    private final byte[] salt = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
    };
    private final int iterationCount = 19;

    public String Encrypted(String plainText) {
         return encrypt(encrypt(encrypt(plainText)));
    }
    //mahoa_md5
    public String encrypt(String plaintext)
    {
         try {
            
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plaintext.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while(hashtext.length() < 32 ){
                
                hashtext = "0"+hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Class_EncryptedDecrypted.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null ;
    }

    public String Decrypted(String encryptedText) {
        if (encryptedText != null && !encryptedText.isEmpty()) {
            try {
                //Key generation for enc and desc
                KeySpec keySpec = new PBEKeySpec(key.toCharArray(), salt, iterationCount);
                SecretKey scr_key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
                // Prepare the parameter to the ciphers
                AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
                //Decryption process; same key will be used for decr
                dcipher = Cipher.getInstance(scr_key.getAlgorithm());
                dcipher.init(Cipher.DECRYPT_MODE, scr_key, paramSpec);
                byte[] enc = new sun.misc.BASE64Decoder().decodeBuffer(encryptedText);
                byte[] utf8 = dcipher.doFinal(enc);
                String charSet = "UTF-8";
                String plainStr = new String(utf8, charSet);
                return plainStr;
            } catch (NoSuchAlgorithmException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (InvalidKeySpecException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (NoSuchPaddingException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (InvalidKeyException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (InvalidAlgorithmParameterException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (IOException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (IllegalBlockSizeException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            } catch (BadPaddingException ex) {
//                Class_InsertErrorApp.insertErrorApp("Decrypted", ex.getStackTrace()[0].getMethodName(), ex.toString());
            }
        }
        return null;
    }
}
