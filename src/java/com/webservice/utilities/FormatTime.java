/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webservice.utilities;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author OSSTECH
 */
public class FormatTime {

    static SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    public static Timestamp convert_1(String HH_MM) {
        try {
            Calendar cal = new GregorianCalendar();
            sd.format(new Date(cal.getTimeInMillis()));
            int d = cal.get(Calendar.DAY_OF_MONTH);
            int m = cal.get(Calendar.MONTH) + 1;
            int y = cal.get(Calendar.YEAR);
            Date date = sd.parse(d + "/" + m + "/" + y + " " + HH_MM + ":00");
            return new Timestamp(date.getTime());
        } catch (ParseException ex) {
            System.out.println("-------convert timestamp is error----------");
        }
        return null;
    }

    public static Timestamp convert(String ThoiGian) {
        SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = sd1.parse(ThoiGian);
            return new Timestamp(date.getTime());
        } catch (ParseException ex) {
            System.out.println("-------convert timestamp is error----------");
        }
        return null;
    }

    public static Timestamp addDay(Timestamp tg) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(tg.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 1);
        Timestamp r = new Timestamp(cal.getTimeInMillis());
        return r;
    }

    public static String getDateSystem() {
        String Date = "";
        Date = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date())
                .toString();
        return Date;
    }
    //Test
//    public static void main(String[] args) {
//        String tgServer = new SimpleDateFormat("yyyy-MM-dd").format(
//                            new java.util.Date()).toString();
//                   
//                    String tgServer2 = tgServer + " 08:32:00";
//                    Timestamp tg2 = FormatTime.convert(tgServer2);
//        Timestamp tg=addDay(tg2);
//    }
}